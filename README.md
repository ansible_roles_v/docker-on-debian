Docker for Debian Linux
=========
Installs docker-ce with requirements.  

Include role
------------
```yaml
- name: docker  
  src: https://gitlab.com/ansible_roles_v/docker/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: servers
  gather_facts: true
  become: true
  roles:
    - docker
```