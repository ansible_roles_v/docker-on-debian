import os
import pytest
import testinfra.utils.ansible_runner

PACKAGES = ['docker-ce']
SERVICES = ['docker']
DOCKER_USER = 'vagrant'

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

@pytest.mark.parametrize('pkg', PACKAGES)
def test_pkg(host, pkg):
    package_objects = host.package(pkg)
    assert package_objects.is_installed

@pytest.mark.parametrize('svc', SERVICES)
def test_svc(host, svc):
    service_object = host.service(svc)
    assert service_object.is_running
    assert service_object.is_enabled

def test_vagrant_user_is_part_of_group_docker(host):
    user_vagrant = host.user(DOCKER_USER)
